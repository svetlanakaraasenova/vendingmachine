import org.example.Product;
import org.example.VendingMachine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VendingMachineTest {
    private VendingMachine vendingMachine;
    private Product product1;
    private Product product2;

    @BeforeEach
    void setUp() {
        vendingMachine = new VendingMachine();
        product1 = new Product("Soda", 1.5);
        product2 = new Product("Chips", 1.0);
        vendingMachine.addProduct(product1);
        vendingMachine.addProduct(product2);
    }

    @Test
    void testAddProduct() {
        Product product3 = new Product("Candy", 0.75);
        vendingMachine.addProduct(product3);

        assertEquals(3, vendingMachine.getProducts().size());
        assertTrue(vendingMachine.getProducts().contains(product3));
    }

    @Test
    void testRemoveProduct() {
        vendingMachine.removeProduct(product1);

        assertEquals(1, vendingMachine.getProducts().size());
        assertFalse(vendingMachine.getProducts().contains(product1));
    }

    @Test
    void testUpdateProductPrice() {
        vendingMachine.updateProductPrice(product1, 2.0);

        assertEquals(2.0, product1.getPrice());
    }

    @Test
    void testEnableMaintenanceMode() {
        vendingMachine.enableMaintenanceMode();

        assertTrue(vendingMachine.isMaintenanceMode());
    }

    @Test
    void testDisableMaintenanceMode() {
        vendingMachine.enableMaintenanceMode();
        vendingMachine.disableMaintenanceMode();

        assertFalse(vendingMachine.isMaintenanceMode());
    }
}
