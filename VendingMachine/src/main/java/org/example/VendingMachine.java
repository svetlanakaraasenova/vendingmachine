package org.example;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class VendingMachine {

    private List<Product> products;

    private boolean maintenanceMode;

    public VendingMachine() {
        this.products = new ArrayList<>();
        this.maintenanceMode = false;
    }

    public void addProduct (Product product){
        this.products.add(product);
    }

    public void removeProduct (Product product){
        this.products.remove(product);
    }

    public void updateProductPrice (Product product, double newPrice){
        product.setPrice(newPrice);
    }

    public void enableMaintenanceMode() {
        maintenanceMode = true;
        System.out.println("Vending machine is now in maintenance mode.");
    }

    public void disableMaintenanceMode() {
        maintenanceMode = false;
        System.out.println("Vending machine is now in operational mode.");
    }

    public void purchase(Product product, double coins) {
        if (maintenanceMode) {
            System.out.println("Vending machine is currently in maintenance mode. Please try again later.");
            return;
        }

        if (coins >= product.getPrice()) {
            double change = coins - product.getPrice();
            System.out.println("Thank you for your purchase of " + product.getName() + ".");
            System.out.println("Your change: " + change);
        } else {
            System.out.println("Insufficient coins. Please insert more coins.");
        }
    }

    public void displayProducts() {
        System.out.println("Available products in the vending machine:");
        for (Product product : products) {
            System.out.println(product.getName() + " - Price: " + product.getPrice());
        }
    }

    public List<Product> getProducts() {
        return this.products;
    }

    public boolean isMaintenanceMode() {
        return maintenanceMode;
    }
}
