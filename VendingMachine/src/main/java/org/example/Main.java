package org.example;

public class Main {
    public static void main(String[] args) {
        VendingMachine vendingMachine = new VendingMachine();

        Product product1 = new Product("Soda", 1.5);
        Product product2 = new Product("Chips", 1.0);
        Product product3 = new Product("Candy", 0.75);
        vendingMachine.addProduct(product1);
        vendingMachine.addProduct(product2);
        vendingMachine.addProduct(product3);

        vendingMachine.displayProducts();

        vendingMachine.purchase(product1, 2.0);

        vendingMachine.enableMaintenanceMode();

        vendingMachine.purchase(product2, 1.0);

        vendingMachine.disableMaintenanceMode();

        vendingMachine.purchase(product2, 1.0);
    }
}